use std::env;
use std::ffi::OsStr;
use std::path::Path;
pub struct Config {
  pub spacing: u32,
  pub count: u32,
  pub ratio: f32,
  pub linecap: String,
  pub width: f32,
  pub color: String,
  pub background: String,
  pub filename: String,
}

impl Config {
  pub fn size(&self) -> u32 {
    (self.count / 2) * 2 * self.spacing
  }
}

pub fn parse_args() -> Result<Config, String> {
  let mut config = Config {
    spacing: 8,
    count: 14,
    ratio: 0.5,
    linecap: String::from("round"),
    width: 1.0,
    color: String::from("#f0f0f0"),
    background: String::from("#1a1a1a"),
    filename: String::from(""),
  };

  let mut args = env::args();
  args.next();
  while let Some(x) = args.next() {
    match x.as_str() {
      "--help" => {
        print_help();
        std::process::exit(0);
      }
      "-h" => {
        print_help();
        std::process::exit(0);
      }
      "--count" => {
        if let Some(next) = args.next() {
          let res = next.parse();
          if res.is_ok() {
            config.count = res.unwrap();
            continue;
          }
        }
        return Err(String::from(
          "Flag \"--count\" requires an integer argument",
        ));
      }
      "--ratio" => {
        if let Some(next) = args.next() {
          let res = next.parse();
          if res.is_ok() {
            let rat = res.unwrap();
            if rat >= 0.0 && rat <= 1.0 {
              config.ratio = rat;
              continue;
            }
          }
        }
        return Err(String::from(
          "Flag \"--ratio\" requires a float argument between 0 and 1",
        ));
      }
      "--linecap" => {
        if let Some(next) = args.next() {
          if next == "butt" || next == "round" || next == "square" {
            config.linecap = next;
            continue;
          }
        }
        return Err(String::from(
          "Flag \"--linecap\" requires an argument between \"butt|round|square\"",
        ));
      }
      "--width" => {
        if let Some(next) = args.next() {
          let res = next.parse();
          if res.is_ok() {
            let rat = res.unwrap();
            if rat >= 0.0 {
              config.width = rat;
              continue;
            }
          }
        }
        return Err(String::from("Flag \"--width\" requires a float argument"));
      }
      "--color" => {
        if let Some(next) = args.next() {
          if is_hex_color(&next) {
            config.color = String::from("#");
            config.color.push_str(&next);
            continue;
          }
        }
        return Err(String::from(
          "Flag \"--color\" requires an hexadecimal color argument without a '#'",
        ));
      }
      "--background" => {
        if let Some(next) = args.next() {
          if is_hex_color(&next) {
            config.background = String::from("#");
            config.background.push_str(&next);
            continue;
          }
        }
        return Err(String::from(
          "Flag \"--background\" requires an hexadecimal color argument without a '#'",
        ));
      }
      _ => {
        config.filename = x;
      }
    }
  }
  if config.filename == "" {
    Err(String::from(
      "Missing required positional argumment: output_file_name",
    ))
  } else {
    Ok(config)
  }
}

fn print_help() {
  println!(
    "Usage: {} output_file_name {{options}}",
    env::current_exe()
      .ok()
      .as_ref()
      .map(Path::new)
      .and_then(Path::file_name)
      .and_then(OsStr::to_str)
      .map(String::from)
      .unwrap()
  );
  println!(
    r#"    output_file_name: the name of the file to output to
options:
    -h --help: display this message
    --count {{n}}: the number of segments on each line/column, n must be a positive integer
        default value: 14
    --ratio {{r}}: the probability that a line/column starts with a gap, 0.0 <= r <= 1.0
        default value: 0.5
    --linecap {{cap}}: the style of the lines, one of "butt|round|square"
        default value: round
    --width {{w}}: the width of the lines, must be a positive float
        default value: 1
    --color {{col}}: the color of the lines, expressed as an exidecimal value (without the leading '#') with 6 digits
        default value: f0f0f0
    --background {{col}}: the color of the background, expressed as an exidecimal value (without the leading '#') with 6 digits
        default value: 1a1a1a"#
  );
}

fn is_hex_color(s: &str) -> bool {
  if s.len() != 6 {
    return false;
  }
  i32::from_str_radix(s, 16).is_ok()
}
