use rand::Rng;

use svg::node::element::path::Data;
use svg::node::element::Path;
use svg::Document;

mod lib;

fn main() {
  let mut rng = rand::thread_rng();
  let parsed = lib::parse_args();
  if let Err(e) = parsed {
    println!("Error while parsing arguments: {}", e);
    std::process::exit(1);
  }
  let config = parsed.unwrap();
  let mut data = Data::new().move_to((0, 0));
  for _ in 0..config.count {
    let mut mov = 0;
    if rng.gen_range(0.0..1.0) < config.ratio {
      data = data.move_by((config.spacing, 0));
      mov += config.spacing;
    }
    for _ in 0..config.count / 2 {
      data = data
        .line_by((config.spacing, 0))
        .move_by((config.spacing, 0));
      mov += config.spacing * 2;
    }
    data = data.move_by((-(mov as i32), config.spacing));
  }
  data = data.move_to((0, 0));
  for _ in 0..config.count {
    let mut mov = 0;
    if rng.gen_range(0.0..1.0) < config.ratio {
      data = data.move_by((0, config.spacing));
      mov += config.spacing;
    }
    for _ in 0..config.count / 2 {
      data = data
        .line_by((0, config.spacing))
        .move_by((0, config.spacing));
      mov += config.spacing * 2;
    }
    data = data.move_by((config.spacing, -(mov as i32)));
  }

  let path = Path::new()
    .set("fill", "none")
    .set("stroke", config.color.clone())
    .set("stroke-width", config.width)
    .set("stroke-linecap", config.linecap.clone())
    .set("d", data);

  let document = Document::new()
    .set("viewBox", (0, 0, config.size(), config.size()))
    .set("style", format!("background-color: {}", config.background))
    .add(path);

  println!("Saving svg to \"{}\"", config.filename);
  if let Err(e) = svg::save(config.filename, &document) {
    println!("Error while saving the file: {}", e);
  }
}
